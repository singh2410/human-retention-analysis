#!/usr/bin/env python
# coding: utf-8

# # Predicting which of the employee will quit the job
# #By- Aarush Kumar
# #Dated: June 03,2021

# In[1]:


import numpy as np
import pandas as pd


# In[2]:


hr_df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Human Retention Analysis/hr_data.csv')


# In[3]:


hr_df


# In[4]:


hr_df.shape


# In[5]:


hr_df.size


# In[6]:


hr_df.info()


# In[7]:


hr_df.isnull().sum()


# In[8]:


hr_df['department'].unique()


# In[9]:


hr_df['salary'].unique()


# In[10]:


s_df=pd.read_excel(r'/home/aarush100616/Downloads/Projects/Human Retention Analysis/employee_satisfaction_evaluation.xlsx')


# In[11]:


s_df


# In[12]:


#merging & joining
main_df= hr_df.set_index('employee_id').join(s_df.set_index('EMPLOYEE #'))


# In[13]:


main_df=main_df.reset_index()


# In[14]:


main_df


# In[15]:


main_df.info()


# In[16]:


main_df[main_df.isnull().any(axis=1)]


# In[17]:


main_df.isnull().sum()


# In[18]:


main_df.describe()


# In[19]:


main_df.fillna(main_df.mean(),inplace=True) # fill with mode for low variance


# In[20]:


main_df.isnull().sum()


# In[21]:


main_df.loc[main_df['employee_id']==1340]


# In[22]:


main_df.drop(columns='employee_id',inplace=True)


# In[23]:


main_df


# In[24]:


main_df.groupby('department').sum()


# In[25]:


main_df.groupby('department').mean()


# In[26]:


main_df['left'].value_counts()


# In[27]:


#Data Visualization
import matplotlib.pyplot as plt
import seaborn as sns


# In[28]:


def plot_corr(df,size=10):
    corr=df.corr()
    fig,ax=plt.subplots(figsize=(size,size))
    ax.legend()
    cax=ax.matshow(corr)
    fig.colorbar(cax)
    plt.xticks(range(len(corr.columns)), corr.columns, rotation='vertical')
    plt.yticks(range(len(corr.columns)), corr.columns)
plot_corr(main_df)


# In[29]:


with sns.color_palette('muted'):
    sns.countplot(x=main_df['left'])


# In[30]:


sns.barplot(x='left',y='satisfaction_level',data=main_df)


# In[31]:


sns.barplot(x='promotion_last_5years',y='satisfaction_level',data=main_df,hue='left')


# In[32]:


sns.pairplot(main_df,hue='left')


# In[33]:


#Data Preprocessing
y=main_df[['department','salary']]
y


# In[34]:


from sklearn.preprocessing import LabelEncoder
le=LabelEncoder()
main_df['salary']=le.fit_transform(main_df['salary'])


# In[35]:


le


# In[36]:


main_df


# In[37]:


main_df['department']=le.fit_transform(main_df['department'])
main_df


# In[38]:


X=main_df.drop(['left'],axis=1)
y = main_df.left


# In[39]:


X


# In[40]:


y


# In[41]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3,random_state=89)
scores_dict = {}


# In[42]:


# Decision Tree
from sklearn.metrics import accuracy_score, classification_report


# In[43]:


from sklearn.tree import DecisionTreeClassifier
dt=DecisionTreeClassifier()
dt.fit(X_train,y_train)
prediction_dt = dt.predict(X_test)
accuracy_dt = accuracy_score(y_test,prediction_dt)*100


# In[44]:


print('Accuracy score : ',accuracy_dt)
scores_dict['DecisionTreeClassifier'] = accuracy_dt
print(classification_report(y_test,prediction_dt))


# In[46]:


Category=['Employee will stay','Employee will Leave']


# In[47]:


custom_dt=[[1,500,3,6,0,0.90,0.89,1,8]]


# In[48]:


print(int(dt.predict(custom_dt)))


# In[49]:


Category[int(dt.predict(custom_dt))]


# In[50]:


dt.feature_importances_


# In[51]:


feature_importance=pd.DataFrame(dt.feature_importances_,index=X_train.columns,columns=['Importance']).sort_values('Importance',ascending=False)


# In[52]:


feature_importance


# In[53]:


#KNN
# Data Processing of KNN
from sklearn.preprocessing import StandardScaler
print(list(main_df.columns))


# In[54]:


num_features = list(main_df.columns)
for x in ['salary','department','left']:
    num_features.remove(x)
sc = StandardScaler()
X_train_std=sc.fit_transform(X_train[num_features])
X_test_std=sc.transform(X_test[num_features])


# In[55]:


from sklearn.neighbors import KNeighborsClassifier
k_range=range(1,26)
scores={}
scores_list=[]
bestk=0
h_score = 0
for i in range(1,26):
    clf = KNeighborsClassifier(n_neighbors=i).fit(X_train_std,y_train)
    pred = clf.predict(X_test_std)
    scores[i]=accuracy_score(y_test,pred)
    if scores[i]>h_score:
        h_score = scores[i]
        bestk = i
    scores_list.append(scores[i])
print('Best k is {} with score : {}'.format(bestk,h_score))


# In[56]:


# main model for KNN
knn=KNeighborsClassifier(n_neighbors=bestk)
knn.fit(X_train_std,y_train)
prediction_knn=knn.predict(X_test_std)
accuracy_knn=accuracy_score(y_test,prediction_knn)*100


# In[57]:


print('Accuracy score : ',accuracy_knn)
scores_dict['KNeighborsClassifier'] = accuracy_score(y_test,prediction_knn)*100
print(classification_report(y_test,prediction_knn))


# In[58]:


plt.plot(k_range,scores_list)


# In[59]:


# SVC
from sklearn.svm import SVC
model = SVC().fit(X_train_std,y_train)
pred = model.predict(X_test_std)
svc_accuracy = accuracy_score(y_test,pred)*100
print('Accuracy score : ',svc_accuracy)
scores_dict['SVC'] = svc_accuracy
print(classification_report(y_test,pred))


# In[61]:


get_ipython().system('pip install lightgbm')


# In[62]:


from lightgbm import LGBMClassifier
model = LGBMClassifier(learning_rate=0.03,n_estimators=1000).fit(X_train_std,y_train)
pred = model.predict(X_test_std)
LGBM_accuracy = accuracy_score(y_test,pred)*100
print('Accuracy score : ',LGBM_accuracy)
scores_dict['LGBMClassifier'] = LGBM_accuracy


# In[63]:


algo_name = list(scores_dict.keys())
accuracy_list = list(scores_dict.values())
sns.set(rc={'figure.figsize':(12.4,6.5)})
with sns.color_palette('muted'):
    sns.barplot(x=algo_name,y=accuracy_list)

